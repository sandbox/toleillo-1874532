<!-- ========================== HEADER ========================== -->
<header class="bck darkest padding_top padding_bottom">
  <div class="row text color white shadow">
    <div class="column_4">
      <a href="<?php print url('<front>');?>" class="logo text normal color white"><?php print $site_name; ?></span></a>
    </div>
    <nav class="column_7 text right">
      <?php if ($tabs): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
    </nav>
  </div>

  <?php if ($page['help']): ?>
    <?php //print render($page['help']); ?>
  <?php endif; ?>

  <?php if ($breadcrumb): ?>
    <div id="breadcrumb">
      <?php //print $breadcrumb; ?>
    </div> <!-- //#breadcrumb -->
  <?php endif; ?>

  

</header>

<section class="bck light underline navigation">
      <div class="row text center">
          <nav class="group text bold">
            <?php if ($page['header']): ?>
              <?php print render($page['header']); ?>
            <?php endif; ?>
          </nav>
      </div>
</section>
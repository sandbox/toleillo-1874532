<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix margin_bottom"<?php print $attributes; ?>>

  <h2 class="text color theme"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  
  <?php if ($display_submitted): ?>
    <div class="text right thin">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="text thin margin_bottom"<?php print $content_attributes; ?>>
    <?php
      // We hide the links now so that we can render them later.
      hide($content['links']);
      print render($content);
    ?>
  </div>

  <?php print render($content['links']); ?>

</div>
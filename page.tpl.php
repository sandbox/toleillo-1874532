<?php include ("templates/inc/header.inc"); ?>

<section class="bck light features">

  <?php if ($page['highlighted']): ?>
    <div class="row text center book margin_top">     
      <?php print render($page['highlighted']); ?>
    </div>
  <?php endif; ?>

  <div class="row margin_bottom">
    <?php if ($page['sidebar_first']): ?>
      <div class="column_4 bck light"> 
        <?php print render($page['sidebar_first']); ?>
      </div>
    <?php endif; ?> 

    <?php if ($messages): ?>
      <div class="small active" data-tuktuk="modal">
        <?php print $messages; ?>
      </div>
    <?php endif; ?>
    
    <?php if ($page['content']): ?>
      <div class="<?php print ($page['sidebar_second'] && $page['sidebar_first']) ? 'column4' : 'column_8'; ?> bck light">
          <?php print render($page['content']); ?>
      </div> 
    <?php endif; ?>
    
    <?php if ($page['sidebar_second']): ?>
      <div class="column_4 bck light"> 
        <?php print render($page['sidebar_second']); ?>
      </div>
    <?php endif; ?>
  </div>

</section>


<!-- ========================== PRE FOOTER ========================== -->
<section class="bck theme features">
  <div class="row margin_top margin_bottom">
    <div class="column_8">
      <?php if ($page['pre_footer_left']): ?>
        <?php print render($page['pre_footer_left']); ?>
      <?php endif; ?>
    </div>
    <div class="column_4">
      <?php if ($page['pre_footer_right']): ?>
        <?php print render($page['pre_footer_right']); ?>
      <?php endif; ?>
    </div>
  </div>

</section>


<!-- ========================== FOOTER ========================== -->
<footer class="bck dark text color white">
  <div class="row margin_top margin_bottom">
    <?php if ($page['footer']): ?>
      <?php print render($page['footer']); ?>
    <?php endif; ?>
  </div>
</footer>

